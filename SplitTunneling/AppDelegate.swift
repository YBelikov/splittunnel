import Cocoa
import SystemExtensions
import NetworkExtension
import os.log

@main
class AppDelegate: NSObject, NSApplicationDelegate
{
    private var tunnelManager = NETunnelProviderManager()
    lazy var extensionBundle: Bundle = {

        let extensionsDirectoryURL = URL(fileURLWithPath: "Contents/Library/SystemExtensions", relativeTo: Bundle.main.bundleURL)
        let extensionURLs: [URL]
        do
        {
            extensionURLs = try FileManager.default.contentsOfDirectory(at: extensionsDirectoryURL,
                                                                        includingPropertiesForKeys: nil,
                                                                        options: .skipsHiddenFiles)
        }
        catch let error
        {
            fatalError("Failed to get the contents of \(extensionsDirectoryURL.absoluteString): \(error.localizedDescription)")
        }

        guard let extensionURL = extensionURLs.first else
        {
            fatalError("Failed to find any system extensions")
        }

        guard let extensionBundle = Bundle(url: extensionURL) else
        {
            fatalError("Failed to create a bundle with URL \(extensionURL.absoluteString)")
        }

        return extensionBundle
    }()

    func applicationDidFinishLaunching(_ aNotification: Notification)
    {
        guard let extensionIdentifier = self.extensionBundle.bundleIdentifier else
        {
            os_log(.default, "Problems with extension bundle ID")
            return
        }
        os_log(.default, "SE activation request")
        let activationRequest = OSSystemExtensionRequest.activationRequest(forExtensionWithIdentifier: extensionIdentifier, queue: .main)
        activationRequest.delegate = self
        OSSystemExtensionManager.shared.submitRequest(activationRequest)
    }
     
    func loadTunnelConfiguration(completionHandler: @escaping (Bool) -> Void )
    {
        NETunnelProviderManager.shared().loadFromPreferences { loadError in
            DispatchQueue.main.async {
                var success = true
                if let error = loadError {
                    os_log(.default, "Failed to load the filter configuration: %@", error.localizedDescription)
                    success = false
                }
                completionHandler(success)
            }
        }
    }
    
    private func makeManager() -> NETunnelProviderManager {
        let manager = NETunnelProviderManager()
        manager.localizedDescription = "CustomVPN"

        // Configure a VPN protocol to use a Packet Tunnel Provider
        let proto = NETunnelProviderProtocol()
        
        // This must match an app extension bundle identifier
        proto.providerBundleIdentifier = "com.os33.mac.network.extension"
        proto.serverAddress = "192.168.0.102:9999"
        proto.providerConfiguration = [:]
        manager.protocolConfiguration = proto
        manager.isEnabled = true
        return manager
    }
    
    func enableTunnelConfiguration()
    {
        os_log("Tunnel manager: %@", self.tunnelManager)
        self.tunnelManager.loadFromPreferences { error in
            let newManager = self.makeManager()
            self.tunnelManager = newManager
            self.tunnelManager.saveToPreferences { error in
                if (error == nil)
                {
                    self.tunnelManager.loadFromPreferences { error in
                        if error != nil
                        {
                            return
                        }
                        if self.tunnelManager.connection.status == .disconnected || self.tunnelManager.connection.status == .invalid
                        {
                            do
                            {
                                NSLog("Try To Connect")
                                try self.tunnelManager.connection.startVPNTunnel()
                            }
                            catch
                            {
                                NSLog("Failed to start vpn: \(error)")
                            }
                        }
                        else
                        {
                            NSLog("Try To Disconnect")
                            self.tunnelManager.connection.stopVPNTunnel()
                        }
                    }
                }
                else
                {
                    os_log("ERROR")
                }
            }
        }
    }
    
    func configureProtocol()
    {
        if self.tunnelManager.protocolConfiguration == nil {
            let protocolConfiguration = NETunnelProviderProtocol()
            protocolConfiguration.providerBundleIdentifier = "com.os33.mac.network.extension"
            
            protocolConfiguration.serverAddress = "192.168.0.102"
            
            protocolConfiguration.providerConfiguration = [:]

            self.tunnelManager.protocolConfiguration = protocolConfiguration
            
            if let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String {
                self.tunnelManager.localizedDescription = appName
            }
        }

        tunnelManager.isEnabled = true

    }
    
    func applicationWillTerminate(_ aNotification: Notification)
    {
        self.tunnelManager.connection.stopVPNTunnel()
    }
}

extension AppDelegate: OSSystemExtensionRequestDelegate
{
    func request(_ request: OSSystemExtensionRequest, actionForReplacingExtension existing: OSSystemExtensionProperties, withExtension extension: OSSystemExtensionProperties) -> OSSystemExtensionRequest.ReplacementAction
    {
        os_log("Replacing extension %@ version %@ with version %@", request.identifier, existing.bundleShortVersion, `extension`.bundleShortVersion)
        return .replace
    }
    
    func requestNeedsUserApproval(_ request: OSSystemExtensionRequest)
    {
        os_log("Tunneling extension %@ requires user approval", request.identifier)
    }
    
    func request(_ request: OSSystemExtensionRequest, didFinishWithResult result: OSSystemExtensionRequest.Result)
    {
        guard result == .completed else
        {
            os_log(.default, "Unexpected result for SystemExtension activation request: %@", result.rawValue)
            return
        }
        enableTunnelConfiguration()
    }
    
    func request(_ request: OSSystemExtensionRequest, didFailWithError error: Error)
    {
        os_log("System extension request failed: %@", error.localizedDescription)
    }
}

