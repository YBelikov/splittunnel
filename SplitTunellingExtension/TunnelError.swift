import Foundation

class TunnelError : NSObject, LocalizedError
{
    var desc = ""
    init(description: String)
    {
        self.desc = description
    }
    
    override var description: String
    {
        get
        {
           return "Error: \(desc)"
        }
    }
    
    var errorDescription: String?
    {
        get
        {
            return self.description
        }
    }
    
}
