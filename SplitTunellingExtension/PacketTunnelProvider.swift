import NetworkExtension
import os.log

class PacketTunnelProvider: NEPacketTunnelProvider
{
    
    private var pendingCompletion: ((Error?) -> Void)?
    private var udpSession: NWUDPSession!
    private var tcpSession: NWTCPConnection!
    private var observer: AnyObject?
    private let port = "9999"
    private let hostname = "192.168.0.102"
    private let queue = DispatchQueue(label: "com.os33.extension")
    override func startTunnel(options: [String : NSObject]?, completionHandler: @escaping (Error?) -> Void)
    {
        os_log(.default, "Tunnel is starting...")
        self.pendingCompletion = completionHandler
        os_log(.default, "Tunnel started successfully")
        self.startUDPSession();
        //self.startPacketRedirection()
    }
    
    private func startUDPSession()
    {
        let endpoint = NWHostEndpoint(hostname: self.hostname, port: self.port)
        udpSession = self.createUDPSession(to: endpoint, from: nil)
        os_log("UDP session: %@", self.udpSession)
        guard let sess = self.udpSession else
        {
            let error = createErrorWithDescription(description: "Failed to create UDP connection")
            self.pendingCompletion?(error)
            return
        }
        os_log(.default, "Session created: %@", "\(self.udpSession)")
        self.observer = udpSession.observe(\.state, options: [.new])
        {
            [weak self] session, _ in
            guard let self = self else { return }
            os_log(.default, "Session did update state: %{public}@", "\(session.state)")
            self.queue.async
            {
                self.handleUDPSession(session, didUpdateState: session.state)
            }
        }
    }
    
    private func handleUDPSession(_ session: NWUDPSession, didUpdateState state: NWUDPSessionState)
    {
        switch state
        {
        case .ready:
            os_log(.default, "Session state: ready")
            self.finishSetup()
        case .cancelled:
            os_log(.default, "Session state: cancelled")
        case .failed:
            os_log(.default, "Session state: failed")
        case .invalid:
            os_log(.default, "Session state: invalid")
        case .preparing:
            os_log(.default, "Session state: preparing")
        case .waiting:
            os_log(.default, "Session state: waiting")
        default:
            break
        }
    }
    
    private func finishSetup()
    {
        os_log(.default, "Setup last stage started")
        self.startPacketRedirection()
    }
    
    
    private func startPacketRedirection()
    {
        os_log(.default, "Start packet redirection")
        let settings = NEPacketTunnelNetworkSettings(tunnelRemoteAddress: self.hostname)
        //split tunneling should work here
        let routes = [NEIPv4Route(destinationAddress:  "199.250.203.36", subnetMask: "255.255.255.255"), NEIPv4Route(destinationAddress: "104.16.41.35", subnetMask: "255.255.255.255"), NEIPv4Route(destinationAddress: "140.82.121.3", subnetMask: "255.255.255.255"),NEIPv4Route(destinationAddress: "140.82.121.4", subnetMask: "255.255.255.255"), NEIPv4Route(destinationAddress: "104.16.42.35", subnetMask: "255.255.255.255")]
        settings.ipv4Settings = NEIPv4Settings(addresses: ["192.168.0.102"], subnetMasks: ["255.255.255.0"] )
        settings.ipv4Settings?.includedRoutes = routes
        if settings.ipv4Settings == nil
        {
            os_log(.default, "Settings is absent")
        }
        else
        {
            os_log(.default, "Settings is: %@", settings)
        }
        setTunnelNetworkSettings(settings)
        { error in
            if let err = error
            {
                os_log(.default, "Error in tunnel settings setup: %@", err.localizedDescription)
                return
            }
            self.pendingCompletion?(nil)
            self.pendingCompletion = nil
            self.readPackets();
        }
    }
    
    private func readPackets()
    {
        os_log(.default, "Package reading started")
        self.packetFlow.readPackets
        {  packets, protocols in
            os_log("Packets received from tun interface %@", packets.count)
            os_log("Protocols received from tun interface %@", protocols.count)
            /*
            Here the troubles with udpSession. Currently it state is always 'failed'
            self.udpSession.writeMultipleDatagrams(packets)
            {
                error in
                    if error != nil
                    {
                        os_log("Error on datagrams writing: %@", "\(error?.localizedDescription)")
                        return
                    }
            }
            self.readPackets()
            */
            
        }
    }
    
    private func createErrorWithDescription(description: String) -> TunnelError
    {
        return TunnelError(description: description)
    }
    
    override func stopTunnel(with reason: NEProviderStopReason, completionHandler: @escaping () -> Void)
    {
        // Add code here to start the process of stopping the tunnel.
        os_log("Stopping the tunnel")
        completionHandler()
    }
    
    override func handleAppMessage(_ messageData: Data, completionHandler: ((Data?) -> Void)?)
    {
        // Add code here to handle the message.
        if let handler = completionHandler
        {
            handler(messageData)
        }
    }
}
